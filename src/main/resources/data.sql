INSERT INTO `pays` (`id_pays`, `nom_pays`) VALUES (1, 'Maroc');


INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (1, 'Aïn Harrouda', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (2, 'Ben Yakhlef', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (3, 'Bouskoura', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (4, 'Casablanca', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (5, 'Médiouna', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (6, 'Mohammadia', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (7, 'Tit Mellil', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (8, 'Ben Yakhlef', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (9, 'Bejaâd', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (10, 'Ben Ahmed', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (11, 'Benslimane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (12, 'Berrechid', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (13, 'Boujniba', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (14, 'Boulanouare', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (15, 'Bouznika', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (16, 'Deroua', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (17, 'El Borouj', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (18, 'El Gara', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (19, 'Guisser', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (20, 'Hattane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (21, 'Khouribga', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (22, 'Loulad', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (23, 'Oued Zem', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (24, 'Oulad Abbou', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (25, 'Oulad H\'Riz Sahel', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (26, 'Oulad M\'rah', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (27, 'Oulad Saïd', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (28, 'Oulad Sidi Ben Daoud', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (29, 'Ras El Aïn', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (30, 'Settat', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (31, 'Sidi Rahhal Chataï', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (32, 'Soualem', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (33, 'Azemmour', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (34, 'Bir Jdid', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (35, 'Bouguedra', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (36, 'Echemmaia', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (37, 'El Jadida', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (38, 'Hrara', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (39, 'Ighoud', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (40, 'Jamâat Shaim', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (41, 'Jorf Lasfar', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (42, 'Khemis Zemamra', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (43, 'Laaounate', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (44, 'Moulay Abdallah', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (45, 'Oualidia', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (46, 'Oulad Amrane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (47, 'Oulad Frej', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (48, 'Oulad Ghadbane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (49, 'Safi', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (50, 'Sebt El Maârif', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (51, 'Sebt Gzoula', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (52, 'Sidi Ahmed', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (53, 'Sidi Ali Ban Hamdouche', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (54, 'Sidi Bennour', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (55, 'Sidi Bouzid', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (56, 'Sidi Smaïl', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (57, 'Youssoufia', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (58, 'Fès', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (59, 'Aïn Cheggag', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (60, 'Bhalil', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (61, 'Boulemane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (62, 'El Menzel', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (63, 'Guigou', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (64, 'Imouzzer Kandar', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (65, 'Imouzzer Marmoucha', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (66, 'Missour', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (67, 'Moulay Yaâcoub', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (68, 'Ouled Tayeb', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (69, 'Outat El Haj', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (70, 'Ribate El Kheir', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (71, 'Séfrou', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (72, 'Skhinate', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (73, 'Tafajight', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (74, 'Arbaoua', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (75, 'Aïn Dorij', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (76, 'Dar Gueddari', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (77, 'Had Kourt', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (78, 'Jorf El Melha', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (79, 'Kénitra', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (80, 'Khenichet', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (81, 'Lalla Mimouna', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (82, 'Mechra Bel Ksiri', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (83, 'Mehdia', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (84, 'Moulay Bousselham', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (85, 'Sidi Allal Tazi', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (86, 'Sidi Kacem', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (87, 'Sidi Slimane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (88, 'Sidi Taibi', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (89, 'Sidi Yahya El Gharb', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (90, 'Souk El Arbaa', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (91, 'Akka', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (92, 'Assa', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (93, 'Bouizakarne', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (94, 'El Ouatia', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (95, 'Es-Semara', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (96, 'Fam El Hisn', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (97, 'Foum Zguid', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (98, 'Guelmim', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (99, 'Taghjijt', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (100, 'Tan-Tan', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (101, 'Tata', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (102, 'Zag', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (103, 'Marrakech', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (104, 'Ait Daoud', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (115, 'Amizmiz', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (116, 'Assahrij', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (117, 'Aït Ourir', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (118, 'Ben Guerir', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (119, 'Chichaoua', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (120, 'El Hanchane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (121, 'El Kelaâ des Sraghna', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (122, 'Essaouira', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (123, 'Fraïta', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (124, 'Ghmate', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (125, 'Ighounane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (126, 'Imintanoute', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (127, 'Kattara', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (128, 'Lalla Takerkoust', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (129, 'Loudaya', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (130, 'Lâattaouia', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (131, 'Moulay Brahim', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (132, 'Mzouda', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (133, 'Ounagha', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (134, 'Sid L\'Mokhtar', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (135, 'Sid Zouin', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (136, 'Sidi Abdallah Ghiat', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (137, 'Sidi Bou Othmane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (138, 'Sidi Rahhal', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (139, 'Skhour Rehamna', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (140, 'Smimou', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (141, 'Tafetachte', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (142, 'Tahannaout', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (143, 'Talmest', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (144, 'Tamallalt', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (145, 'Tamanar', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (146, 'Tamansourt', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (147, 'Tameslouht', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (148, 'Tanalt', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (149, 'Zeubelemok', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (150, 'Meknès', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (151, 'Khénifra', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (152, 'Agourai', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (153, 'Ain Taoujdate', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (154, 'MyAliCherif', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (155, 'Rissani', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (156, 'Amalou Ighriben', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (157, 'Aoufous', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (158, 'Arfoud', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (159, 'Azrou', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (160, 'Aïn Jemaa', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (161, 'Aïn Karma', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (162, 'Aïn Leuh', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (163, 'Aït Boubidmane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (164, 'Aït Ishaq', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (165, 'Boudnib', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (166, 'Boufakrane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (167, 'Boumia', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (168, 'El Hajeb', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (169, 'Elkbab', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (170, 'Er-Rich', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (171, 'Errachidia', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (172, 'Gardmit', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (173, 'Goulmima', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (174, 'Gourrama', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (175, 'Had Bouhssoussen', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (176, 'Haj Kaddour', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (177, 'Ifrane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (178, 'Itzer', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (179, 'Jorf', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (180, 'Kehf Nsour', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (181, 'Kerrouchen', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (182, 'M\'haya', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (183, 'M\'rirt', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (184, 'Midelt', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (185, 'Moulay Ali Cherif', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (186, 'Moulay Bouazza', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (187, 'Moulay Idriss Zerhoun', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (188, 'Moussaoua', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (189, 'N\'Zalat Bni Amar', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (190, 'Ouaoumana', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (191, 'Oued Ifrane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (192, 'Sabaa Aiyoun', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (193, 'Sebt Jahjouh', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (194, 'Sidi Addi', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (195, 'Tichoute', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (196, 'Tighassaline', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (197, 'Tighza', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (198, 'Timahdite', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (199, 'Tinejdad', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (200, 'Tizguite', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (201, 'Toulal', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (202, 'Tounfite', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (203, 'Zaouia d\'Ifrane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (204, 'Zaïda', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (205, 'Ahfir', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (206, 'Aklim', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (207, 'Al Aroui', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (208, 'Aïn Bni Mathar', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (209, 'Aïn Erreggada', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (210, 'Ben Taïeb', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (211, 'Berkane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (212, 'Bni Ansar', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (213, 'Bni Chiker', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (214, 'Bni Drar', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (215, 'Bni Tadjite', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (216, 'Bouanane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (217, 'Bouarfa', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (218, 'Bouhdila', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (219, 'Dar El Kebdani', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (220, 'Debdou', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (221, 'Douar Kannine', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (222, 'Driouch', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (223, 'El Aïoun Sidi Mellouk', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (224, 'Farkhana', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (225, 'Figuig', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (226, 'Ihddaden', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (227, 'Jaâdar', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (228, 'Jerada', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (229, 'Kariat Arekmane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (230, 'Kassita', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (231, 'Kerouna', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (232, 'Laâtamna', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (233, 'Madagh', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (234, 'Midar', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (235, 'Nador', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (236, 'Naima', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (237, 'Oued Heimer', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (238, 'Oujda', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (239, 'Ras El Ma', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (240, 'Saïdia', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (241, 'Selouane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (242, 'Sidi Boubker', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (243, 'Sidi Slimane Echcharaa', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (244, 'Talsint', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (245, 'Taourirt', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (246, 'Tendrara', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (247, 'Tiztoutine', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (248, 'Touima', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (249, 'Touissit', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (250, 'Zaïo', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (251, 'Zeghanghane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (252, 'Rabat', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (253, 'Salé', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (254, 'Ain El Aouda', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (255, 'Harhoura', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (256, 'Khémisset', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (257, 'Oulmès', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (258, 'Rommani', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (259, 'Sidi Allal El Bahraoui', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (260, 'Sidi Bouknadel', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (261, 'Skhirate', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (262, 'Tamesna', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (263, 'Témara', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (264, 'Tiddas', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (265, 'Tiflet', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (266, 'Touarga', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (267, 'Agadir', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (268, 'Agdz', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (269, 'Agni Izimmer', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (270, 'Aït Melloul', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (271, 'Alnif', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (272, 'Anzi', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (273, 'Aoulouz', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (274, 'Aourir', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (275, 'Arazane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (276, 'Aït Baha', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (277, 'Aït Iaâza', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (278, 'Aït Yalla', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (279, 'Ben Sergao', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (280, 'Biougra', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (281, 'Boumalne-Dadès', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (282, 'Dcheira El Jihadia', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (283, 'Drargua', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (284, 'El Guerdane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (285, 'Harte Lyamine', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (286, 'Ida Ougnidif', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (287, 'Ifri', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (288, 'Igdamen', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (289, 'Ighil n\'Oumgoun', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (290, 'Imassine', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (291, 'Inezgane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (292, 'Irherm', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (293, 'Kelaat-M\'Gouna', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (294, 'Lakhsas', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (295, 'Lakhsass', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (296, 'Lqliâa', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (297, 'M\'semrir', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (298, 'Massa (Maroc)', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (299, 'Megousse', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (300, 'Ouarzazate', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (301, 'Oulad Berhil', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (302, 'Oulad Teïma', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (303, 'Sarghine', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (304, 'Sidi Ifni', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (305, 'Skoura', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (306, 'Tabounte', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (307, 'Tafraout', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (308, 'Taghzout', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (309, 'Tagzen', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (310, 'Taliouine', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (311, 'Tamegroute', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (312, 'Tamraght', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (313, 'Tanoumrite Nkob Zagora', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (314, 'Taourirt ait zaghar', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (315, 'Taroudannt', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (316, 'Temsia', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (317, 'Tifnit', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (318, 'Tisgdal', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (319, 'Tiznit', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (320, 'Toundoute', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (321, 'Zagora', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (322, 'Afourar', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (323, 'Aghbala', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (324, 'Azilal', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (325, 'Aït Majden', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (326, 'Beni Ayat', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (327, 'Béni Mellal', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (328, 'Bin elouidane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (329, 'Bradia', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (330, 'Bzou', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (331, 'Dar Oulad Zidouh', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (332, 'Demnate', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (333, 'Dra\'a', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (334, 'El Ksiba', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (335, 'Foum Jamaa', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (336, 'Fquih Ben Salah', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (337, 'Kasba Tadla', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (338, 'Ouaouizeght', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (339, 'Oulad Ayad', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (340, 'Oulad M\'Barek', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (341, 'Oulad Yaich', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (342, 'Sidi Jaber', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (343, 'Souk Sebt Oulad Nemma', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (344, 'Zaouïat Cheikh', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (345, 'Tanger', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (346, 'Tétouan', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (347, 'Akchour', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (348, 'Assilah', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (349, 'Bab Berred', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (350, 'Bab Taza', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (351, 'Brikcha', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (352, 'Chefchaouen', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (353, 'Dar Bni Karrich', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (354, 'Dar Chaoui', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (355, 'Fnideq', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (356, 'Gueznaia', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (357, 'Jebha', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (358, 'Karia', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (359, 'Khémis Sahel', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (360, 'Ksar El Kébir', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (361, 'Larache', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (362, 'Mdiq', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (363, 'Martil', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (364, 'Moqrisset', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (365, 'Oued Laou', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (366, 'Oued Rmel', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (367, 'Ouazzane', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (368, 'Point Cires', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (369, 'Sidi Lyamani', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (370, 'Sidi Mohamed ben Abdallah el-Raisuni', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (371, 'Zinat', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (372, 'Ajdir', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (373, 'Aknoul', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (374, 'Al Hoceïma', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (375, 'Aït Hichem', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (376, 'Bni Bouayach', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (377, 'Bni Hadifa', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (378, 'Ghafsai', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (379, 'Guercif', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (380, 'Imzouren', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (381, 'Inahnahen', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (382, 'Issaguen (Ketama)', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (383, 'Karia (El Jadida)', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (384, 'Karia Ba Mohamed', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (385, 'Oued Amlil', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (386, 'Oulad Zbair', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (387, 'Tahla', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (388, 'Tala Tazegwaght', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (389, 'Tamassint', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (390, 'Taounate', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (391, 'Targuist', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (392, 'Taza', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (393, 'Taïnaste', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (394, 'Thar Es-Souk', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (395, 'Tissa', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (396, 'Tizi Ouasli', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (397, 'Laayoune', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (398, 'El Marsa', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (399, 'Tarfaya', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (400, 'Boujdour', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (401, 'Awsard', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (402, 'Oued-Eddahab', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (403, 'Stehat', 1);
INSERT INTO `villes` (`id_ville`, `nom_ville`, `pays_id_pays`) VALUES (404, 'Aït Attab', 1);

INSERT INTO `specialites` (`id_spec`, `libelle`) VALUES (1, 'Généraliste');
INSERT INTO `specialites` (`id_spec`, `libelle`) VALUES (2, 'Dentiste');


INSERT INTO `roles` (`id`, `name`) VALUES (1, 'ROLE_USER');
INSERT INTO `roles` (`id`, `name`) VALUES (2, 'ROLE_MODERATOR');
INSERT INTO `roles` (`id`, `name`) VALUES (3, 'ROLE_ADMIN');


INSERT INTO `types_doc` (`id_typedoc`, `code_type`, `type_doc`) VALUES (1, '1', 'Ordonance');

INSERT INTO `cabinet` (`id_cabinet`, `adresse`, `code_post`, `horaires`, `nom`) VALUES
(1, '1 rue de france', '10000', '2021-06-08 09:53:26.000000', 'Cabinet médical Sousou'),
(2, '33 Rue Najib Mahfoud, Casablanca 20000, Morocco', '123456', NULL, 'Central Hospital');


INSERT INTO `users` (`type_user`, `id`, `creation_date_time`, `email`, `email_verified`, `last_login`, `password`, `provider`, `telephone`) VALUES
('User', 1, NULL, 'apocair8@gmail.com', b'0', NULL, '$2a$10$Ns4iMhB/35DUG22CgrFCL.prNxOrQ3ggmThszl2L6y3fgXk5SwfX.', NULL, '0651214511'),
('PA', 16, NULL, 'sakl@afaqdesigns.com', b'1', '2022-01-11 21:07:28.000000', '$2a$10$vW48zWZCZyR4myMZGAlpt.u6hQFTs40xX1ZaBDlyRFyrld2Lpzm8O', NULL, '123456'),
('User', 20, '2021-08-25 01:17:23.000000', 'shadyakl89@gmail.com', b'1', '2021-08-25 01:17:23.000000', NULL, 'FACEBOOK', NULL),
('PA', 23, '2021-09-03 08:48:50.000000', 'nelly@afaqdesigns.com', b'1', '2021-09-25 20:31:57.000000', '$2a$10$vW48zWZCZyR4myMZGAlpt.u6hQFTs40xX1ZaBDlyRFyrld2Lpzm8O', NULL, '022763552');


INSERT INTO `secretaires` (`adresse`, `date_naissance`, `nom`, `prenom`, `id`, `id_cabinet`) VALUES
('test ', '2018-04-05 22:00:47.000000', 'Emmy', 'Adams', 1, 1);


INSERT INTO `cabinet_secretaires` (`cabinet_id_cabinet`, `secretaires_id`) VALUES
(1, 1);



INSERT INTO `medecins` (`civilite_med`, `description_med`, `diplome_med`, `experience_med`, `matricule_med`, `nom_med`, `photo_med`, `prenom_med`, `id`, `id_cabinet`, `specialite_id_spec`, `ville_id_ville`) VALUES
('Mr', 'test 1', 'test2', 'test3', 'test4', 'test6', 'https://f.hellowork.com/obs-static-images/seo/ObsJob/medecin-du-travail.jpg', 'test8', 1, 1, 1, 1),
('Mr', 'description', 'diplome', 'Consultant', 'matricule', 'test11', 'https://findado.osteopathic.org/wp-content/uploads/2018/01/dr-mike-1-e1591905960819-300x199.jpg', 'test12', 20, 2, 2, 1);


INSERT INTO `moyenspaiement` (`id_mp`, `type`) VALUES
(1, 'Cash'),
(2, 'Credit');


INSERT INTO `medecins_moyen_paiement` (`medecin_id`, `moyen_paiement_id_mp`) VALUES
(1, 1),
(1, 2); 

INSERT INTO `type_consultation` (`consultation_id`, `period`, `title`, `medecin_id`) VALUES
(129, 'MIN10', 'Test After Simplifying', 1);

INSERT INTO `medecin_schedule` (`id`, `availability_end`, `availability_start`, `day`, `medecin_id`, `type_consultation_id`) VALUES
(1, '2021-09-06 10:00:00.000000', '2021-09-06 08:00:00.000000', 0, 1, 129),
(2, '2021-09-03 15:00:00.000000', '2021-09-03 13:00:00.000000', 4, 1, 129),
(3, '2021-09-04 10:00:00.000000', '2021-09-04 08:00:00.000000', 5, 1, 129),
(4, '2021-09-04 18:30:00.000000', '2021-09-04 16:00:00.000000', 5, 1, 129);


INSERT INTO `dossiers` (`id_dossier`, `dossier_type`, `num_dossier`, `traitement`) VALUES
(145, 'PORCHE', '145', b'0'),
(146, 'MOI', '146', b'0');

INSERT INTO `patients` (`adresse_pat`, `civilite_pat`, `code_post_pat`, `date_naissance`, `matricule_pat`, `mutuel_number`, `nom_pat`, `patient_email`, `patient_telephone`, `patient_type`, `place_of_birth`, `prenom_pat`, `id`, `dossier_medical_id_dossier`, `id_user`, `id_ville`) VALUES
('asd', 'Mr', '11900', '2021-09-03 16:34:01.000000', 'string', '01005145489', 'Shady', 'sakl@afaqdesgisns.com', '022763552', 'MOI', 'Cairo', 'Akl', 16, 145, 16, 1),
('string', 'Mr', 'string', '2021-09-03 16:34:01.000000', 'string', '01005135489', 'Yahia', 'nelly@afaqdesgisns.com', '022763552', 'PORCHE', 'Cairo', 'El Sayed', 23, 145, 16, 1);


INSERT INTO `rendezvous` (`id`, `canceled_at`, `day`, `end`, `start`, `statut`, `medecin`, `patient`, `type_consultation_id`) VALUES
(1, NULL, 2, '2022-01-25 13:40:00.000000', '2022-01-23 13:30:00.000000', NULL, 1, 23, 129),
(136, NULL, 1, '2022-01-24 16:20:00.000000', '2022-01-24 16:10:00.000000', NULL, 1, 23, 129),
(137, NULL, 3, '2021-01-26 11:30:00.000000', '2021-10-01 11:20:00.000000', NULL, 1, 16, 129);


INSERT INTO `documents` (`id_doc`, `archived`, `auteur`, `date_ajout_doc`, `fichier_doc`, `numero_doc`, `titre_doc`, `id_dossier`, `patient_id`, `rdv_id`, `type_doc`) VALUES
(2, 0, 'Dr. Adam', '2021-09-27 22:30:28.000000', 'https://documentspatient.blob.core.windows.net/documentspatient/2.PNG', 2, 'Test doc2', 145, 16, 137, 1),
(3, 0, 'Dr. Sara', '2021-09-27 23:16:40.000000', 'https://documentspatient.blob.core.windows.net/documentspatient/3.png', 3, 'Test Doc 1', 145, 16, 137, 1),
(4, 1, 'Dr. Joe', '2021-09-29 23:16:40.000000', 'https://documentspatient.blob.core.windows.net/documentspatient/3.png', 4, 'Test Doc Arch', 146, 16, NULL, 1),
(5, 0, 'Dr. Joe', '2021-09-29 23:16:40.000000', 'https://documentspatient.blob.core.windows.net/documentspatient/2.png', 5, 'Prescription by doctor', 146, 16, NULL, 1),
(6, 0, NULL, '2021-10-01 01:20:38.000000', 'https://documentspatient.blob.core.windows.net/documentspatient/6.PNG', 6, 'Test add from all docs', 146, 16, NULL, 1),
(9, 0, NULL, '2022-01-11 21:14:19.000000', NULL, NULL, 'Test doc2', 145, 23, 136, 1);


INSERT INTO `documents_medecins` (`document_id_doc`, `medecins_id`) VALUES
(5, 1);




