package com.clinital.payload.response;

import com.clinital.enums.ConsultationPeriodEnum;

import lombok.Data;

@Data
public class TypeConsultationResponse {

	private Long consultationId;
	private String title;
	private ConsultationPeriodEnum period;
	private Long medecinId;

}
