package com.clinital.payload.response;

import java.time.LocalDateTime;
import java.util.Date;

import com.clinital.enums.RdvStatutEnum;

import lombok.Data;

@Data
public class RendezvousResponse {
	private Long id;
	private Date jour;
	private LocalDateTime start;
	private LocalDateTime end;
	private LocalDateTime canceledAt;
	private MedecinResponse medecin;
	private PatientResponse patient;
	private TypeConsultationResponse typeConsultation;
	private RdvStatutEnum statut;


}
