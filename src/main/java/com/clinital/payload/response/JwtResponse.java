package com.clinital.payload.response;

import java.util.List;

import lombok.Data;

@Data
public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private Long id;
	private String email;
	private String telephone;
	private List<String> roles;

	public JwtResponse(String accessToken, Long id, String email, String telephone, List<String> roles) {
		this.token = accessToken;
		this.id = id;
		this.email = email;
		this.telephone = telephone;
		this.roles = roles;
	}

}
