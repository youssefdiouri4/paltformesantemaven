package com.clinital.payload.response;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class AgendaResponse {

	DayOfWeek day;

	List<GeneralResponse> medecinTimeTable = new ArrayList<GeneralResponse>();

	List<HorairesResponse> workingHours = new ArrayList<HorairesResponse>();

	List<String> availableSlot = new ArrayList<String>();

	public AgendaResponse(DayOfWeek day, List<String> availableSlot) {
		this.day = day;
		this.availableSlot = availableSlot;
	}

	public AgendaResponse() {
	}

}
