package com.clinital.payload.request;

import java.time.DayOfWeek;
import java.time.LocalDateTime;

import lombok.Data;

@Data
public class RendezvousRequest {

	private Long id;
	private DayOfWeek jour;
	private LocalDateTime start;
	private LocalDateTime end;
	private Long medecinId;
	private Long patientId;
	private Long typeConsultationId;
//	private Long medecinScheduleId;

}
