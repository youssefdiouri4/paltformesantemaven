package com.clinital.payload.request;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.clinital.controllers.MedecinController.MedecinParams;

import lombok.Data;

@Data
public class MedecinScheduleRequest {

	private Long id;
	private LocalDate day;
	private LocalDateTime availabilityStart;
	private LocalDateTime availabilityEnd;
	private MedecinParams medecin;
}
