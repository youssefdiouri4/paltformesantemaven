package com.clinital.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.clinital.enums.PatientTypeEnum;

import lombok.Data;

@Entity
@Table(name = "dossiers")
@Data
public class DossierMedical {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id_dossier;
	private String numDossier;
	private boolean traitement;
	@Enumerated(value = EnumType.STRING)
	@Column(name = "dossier_type")
	private PatientTypeEnum dossierType;
	@OneToMany
	private List<Document> documents;

	public DossierMedical() {
		super();
	}

	public DossierMedical(String numDossier, boolean traitement, List<Document> documents) {
		super();
		this.numDossier = numDossier;
		this.traitement = traitement;
		this.documents = documents;
	}

}
