package com.clinital.models;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.format.annotation.DateTimeFormat;

import com.clinital.enums.RdvStatutEnum;

import lombok.Data;

@Entity
@Table(name = "rendezvous")
@Data
public class Rendezvous {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "day")
	@Enumerated(value = EnumType.ORDINAL)
	private DayOfWeek day;
//	private String motif;
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private LocalDateTime start;
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private LocalDateTime end;
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private LocalDateTime canceledAt;

	@Enumerated(EnumType.STRING)
	private RdvStatutEnum statut;

	@JoinColumn(name = "medecin", nullable = false, referencedColumnName = "id", insertable = true, updatable = true)
	@ManyToOne
	private Medecin medecin;

	@JoinColumn(name = "patient", nullable = true, referencedColumnName = "id", insertable = true, updatable = true)
	@ManyToOne
	private Patient patient;

	@JoinColumn(name = "type_consultation_id", nullable = false, referencedColumnName = "consultation_id", insertable = true, updatable = true)
	@ManyToOne
	private TypeConsultation typeConsultation;

	@OneToMany
	private List<Document> documents;

//	@JoinColumn(name = "medecin_schedule_id", nullable = false, referencedColumnName = "id", insertable = true, updatable = true)
//	@ManyToOne
//	private MedecinSchedule medecinSchedule;

	public Rendezvous() {
		super();
	}

	public Rendezvous(DayOfWeek day, @NotBlank String motif, LocalDateTime start, LocalDateTime end,
			LocalDateTime canceledAt, RdvStatutEnum statut, Medecin medecin, Patient patient) {
		super();
		this.day = day;
//		this.motif = motif;
		this.start = start;
		this.end = end;
		this.canceledAt = canceledAt;
		this.statut = statut;
		this.medecin = medecin;
		this.patient = patient;
	}

}
