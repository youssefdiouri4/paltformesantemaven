package com.clinital.models;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Table(name = "cabinet")
@Data
public class Cabinet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_cabinet;
	private String nom;
	private String adresse;
	private String code_post;
	private Date horaires;
	@OneToMany
	private List<Secretaire> secretaires;
	@ManyToMany
	private List<Medecin> medecins;

	public Cabinet() {
		super();
	}

	public Cabinet(@NotNull String nom, @NotNull String adresse, @NotNull String code_post, @NotNull Date horaires,
			List<Secretaire> secretaires, List<Medecin> medecins) {
		super();
		this.nom = nom;
		this.adresse = adresse;
		this.code_post = code_post;
		this.horaires = horaires;
		this.secretaires = secretaires;
		this.medecins = medecins;
	}

}
