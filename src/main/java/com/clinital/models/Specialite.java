package com.clinital.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Entity
@Table(name = "specialites")
@Data
public class Specialite {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_spec;

	private String libelle;

	public Specialite() {
		super();
	}

	public Specialite(Long id_spec, @NotBlank String libelle) {
		super();
		this.id_spec = id_spec;
		this.libelle = libelle;
	}

}
