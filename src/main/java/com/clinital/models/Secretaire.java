package com.clinital.models;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Table(name = "secretaires")
@DiscriminatorValue("SC")
@Data
public class Secretaire extends User {

	private String nom;
	private String prenom;
	private Date dateNaissance;
	private String adresse;

	@ManyToOne
	@JoinColumn(name = "id_cabinet", nullable = false, referencedColumnName = "id_cabinet", insertable = true, updatable = true)
	private Cabinet cabinet;

	public Secretaire() {
		super();
	}

	public Secretaire(@NotNull String nom, @NotNull String prenom, @NotNull Date dateNaissance, @NotNull String adresse,
			Cabinet cabinet) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.adresse = adresse;
		this.cabinet = cabinet;
	}

}
