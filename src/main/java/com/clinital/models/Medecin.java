package com.clinital.models;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.clinital.enums.CiviliteEnum;

import lombok.Data;

@Entity
@Table(name = "medecins")
@DiscriminatorValue("MD")
@Data
public class Medecin extends User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String matricule_med;
	private String nom_med;
	private String prenom_med;
	private String photo_med;
	private String diplome_med;
	private String experience_med;
	private String description_med;
	@Enumerated(value = EnumType.STRING)
	private CiviliteEnum civilite_med;
	@ManyToOne
	private Ville ville;
	@ManyToOne
	private Specialite specialite;
//	@OneToMany
//	private List<Rendezvous> lesrdvs;
	@ManyToOne
	@JoinColumn(name = "id_cabinet", nullable = false, referencedColumnName = "id_cabinet", insertable = true, updatable = true)
	private Cabinet cabinet;

	@OneToMany
	private List<MoyenPaiement> moyenPaiement;

//	@OneToMany
//	private List<TypeConsultation> typeConsultations;

	public Medecin() {
		super();
	}

	public Medecin(@NotBlank String matricule_med, String nom_med, @NotBlank String prenom_med,
			@NotBlank String photo_med, @NotNull String diplome_med, @NotNull String experience_med,
			@NotNull String description_med, CiviliteEnum civilite_med, Ville ville, Specialite specialite,
			List<Rendezvous> lesrdvs, Cabinet cabinet) {
		super();
		this.matricule_med = matricule_med;
		this.nom_med = nom_med;
		this.prenom_med = prenom_med;
		this.photo_med = photo_med;
		this.diplome_med = diplome_med;
		this.experience_med = experience_med;
		this.description_med = description_med;
		this.civilite_med = civilite_med;
		this.ville = ville;
		this.specialite = specialite;
//		this.lesrdvs = lesrdvs;
		this.cabinet = cabinet;
	}

}
