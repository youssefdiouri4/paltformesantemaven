package com.clinital.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "consultations")
@Data
public class Consultation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id_consultation;
	private String num_consultation;
	private Date date;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_medecin", nullable = false, referencedColumnName = "id", insertable = true, updatable = true)
	private Medecin medecin;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_patient", nullable = false, referencedColumnName = "id", insertable = true, updatable = true)
	private Patient patient;
	
	public Consultation() {
		super();
	}
	
	public Consultation(String num_consultation, Date date, Medecin medecin, Patient patient) {
		super();
		this.num_consultation = num_consultation;
		this.date = date;
		this.medecin = medecin;
		this.patient = patient;
	}

	
	
}
