package com.clinital.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.clinital.models.TypeDocument;

public interface TypeDocumentRepository extends JpaRepository<TypeDocument, Long> {

}
