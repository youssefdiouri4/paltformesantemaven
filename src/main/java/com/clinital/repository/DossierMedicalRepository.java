package com.clinital.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.clinital.models.DossierMedical;

public interface DossierMedicalRepository extends JpaRepository<DossierMedical, Long> {

}
