package com.clinital.repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.clinital.models.Rendezvous;

@Repository
public interface RdvRepository extends JpaRepository<Rendezvous, Long> {

	@Query("from Rendezvous r where r.day= ?1")
	List<Rendezvous> getRdvByDate(Date day);

//	@Query("from Rendezvous r where r.motif= ?1")
//	List<Rendezvous> getRdvByMotif(String motif);

	@Query("from Rendezvous r where r.id= ?1")
	List<Rendezvous> getRendezvousById(Long id);

	@Query("from Rendezvous r where r.patient.id = ?1")
	List<Rendezvous> getRdvByIdPatient(Long id);

	@Query("from Rendezvous r where r.patient.nom_pat= ?1")
	List<Rendezvous> getRdvByNomPatient(String nom_pat);

	@Query("from Rendezvous r where r.day =?1 AND r.medecin.nom_med =?2")
	List<Rendezvous> findByDateandMedecin(Date day, String nom_med);

	@Query("select r from Rendezvous r where r.statut = 'CONFIRME' and r.patient.id =?1")
	List<Rendezvous> findConfirmedBypatientId(@Param("id") Long id);

	@Query("select r from Rendezvous r where r.statut = 'CONFIRME' and r.patient.nom_pat =?1")
	List<Rendezvous> findConfirmedBypatientId(@Param("nom_pat") String nom_pat);

	@Query("from Rendezvous r where r.start =?1 AND r.medecin.id =?2")
	List<Rendezvous> findByDateAndMedecin(LocalDateTime dayFrom, Long id);

	@Query("from Rendezvous r where r.patient.user.id= ?1")
	public List<Rendezvous> findAll(Long id);

	@Query("from Rendezvous r where r.medecin.id =?1")
	List<Rendezvous> findByAllRdvByMedecin(Long id);

	List<Rendezvous> findByMedecinCabinetSecretairesId(Long id);

}
