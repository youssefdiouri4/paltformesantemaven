package com.clinital.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.clinital.models.Consultation;
import com.clinital.models.Patient;

@Repository
public interface ConsultationRepository extends JpaRepository<Consultation, Long>{
	
	public Consultation findById(long id_consultation);
	
	public List<Consultation> findAll();
	
	public List<Consultation> findByPatient(Patient patient);
	
//	public void deleteById(long id_consultation);
	
	

}
