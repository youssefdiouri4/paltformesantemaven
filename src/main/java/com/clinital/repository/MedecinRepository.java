package com.clinital.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.clinital.models.Medecin;

@Repository
public interface MedecinRepository extends JpaRepository<Medecin, Long> {
	
//	Optional<Medecin> getMedByName(String nom_med);
//	
	@Query("from Medecin m where m.nom_med = ?1")
	List<Medecin> getMedecinByName(String nom_med);
	
	@Query("from Medecin m where m.id= id")
	List<Medecin> getMedecinById(Long id);
	
	@Query("SELECT m FROM Medecin m, Specialite s , Ville v WHERE s.id_spec = m.specialite.id_spec "
			+ " AND m.ville.id_ville = v.id_ville  AND m.ville.nom_ville = '?2'"
			+ " AND ( m.specialite.libelle LIKE ?1% OR m.nom_med LIKE ?1% ) ")
	List<Medecin> getMedecinBySpecialiteOrName(String search, String ville);
	
	@Query("SELECT m FROM Medecin m, Specialite s , Ville v WHERE s.id_spec = m.specialite.id_spec "
			+ " AND m.ville.id_ville = v.id_ville  AND m.ville.nom_ville = ?1"
			+ " AND ( m.specialite.libelle LIKE ?2%  OR m.nom_med LIKE ?2% ) ")
	List<Medecin> getMedecinBySpecialiteOrNameAndVille(String ville,String search);
	
	@Query("SELECT m FROM Medecin m, Ville v WHERE m.ville.id_ville = v.id_ville AND m.ville.id_ville = ?1")
	List<Medecin> getMedecinByVille(Long id_ville);

	@Query("SELECT m FROM Medecin m, Specialite s WHERE s.id_spec = m.specialite.id_spec"
			+ " AND ( m.specialite.libelle like ?1%  OR m.nom_med like ?1%) ")
	List<Medecin> getMedecinBySpecOrName(String search);
	
	
	
//	, String nom_med, String ville
	
	public List<Medecin> findAll();

}
