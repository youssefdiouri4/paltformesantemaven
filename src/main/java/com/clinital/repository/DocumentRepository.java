package com.clinital.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.clinital.models.Document;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {

	@Query("from Document d where d.patient.id= ?1")
	List<Document> getDocByIdPatient(Long id_patient);

	@Query("from Document d where d.patient.nom_pat= ?1")
	List<Document> getDocByNomPatient(String nom_pat);

	public List<Document> findAll();

	@Query("from Document d where d.id= ?1")
	List<Document> getDocumentById(Long id);

	List<Document> findByPatientId(Long patientId);

	List<Document> findByPatientIdIn(List<Long> patientsId);

	List<Document> findByPatientIdAndArchived(Long patientId, Boolean archived);

	@Query("from Document d where d.rendezvous.id= ?1")
	List<Document> getDocByIdRendezvous(Long rdvId);

	@Query(value = "SELECT documents.* FROM documents  , documents_medecins dm WHERE documents.id_doc=dm.document_id_doc AND documents.patient_id IN(?1)", nativeQuery = true)
	List<Document> getDocByPatientIdAndMedecin(List<Long> patientsId);

//	@Query(value = "SELECT * FROM documents WHERE ", nativeQuery = true)
//	List<Document> getAllDocumentsByPatientId(Long patientId);

}
