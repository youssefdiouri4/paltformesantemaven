package com.clinital.repository;

import java.time.DayOfWeek;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.clinital.models.MedecinSchedule;

public interface MedecinScheduleRepository extends JpaRepository<MedecinSchedule, Long> {

	List<MedecinSchedule> findByMedecinIdAndTypeConsultationConsultationIdAndDay(Long medecinId,
			Long typeConsultationId, DayOfWeek day);

}
