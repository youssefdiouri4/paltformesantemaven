package com.clinital.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.clinital.models.TypeConsultation;

public interface TypeConsultationRepository extends JpaRepository<TypeConsultation, Long> {

	List<TypeConsultation> findByMedecinId(Long medecinId);
}
