package com.clinital.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.clinital.enums.PatientTypeEnum;
import com.clinital.models.Patient;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {

	@Query("from Patient p where p.user.id = ?1")
	public Patient findPatientByAccount(Long userID);

	@Query("from Patient p where p.patient_type = ?1")
	public List<Patient> findPatientByType(PatientTypeEnum type);

	public List<Patient> findAllByUserId(Long userID);
}
