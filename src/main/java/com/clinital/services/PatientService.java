package com.clinital.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.clinital.dao.IDao;
import com.clinital.models.DossierMedical;
import com.clinital.models.Patient;
import com.clinital.repository.DossierMedicalRepository;
import com.clinital.repository.PatientRepository;

@Service
@Primary
public class PatientService implements IDao<Patient> {

	@Autowired
	private PatientRepository patientRepository;

	@Autowired
	private DossierMedicalRepository dossierMedicalRepository;

	@Override
	@Transactional
	public void create(Patient o) {
		DossierMedical dossierMedical = new DossierMedical();
		dossierMedical.setDossierType(o.getPatient_type());
		DossierMedical numberedDossierMedical = dossierMedicalRepository.save(dossierMedical);
		numberedDossierMedical.setNumDossier(numberedDossierMedical.getId_dossier().toString());
		DossierMedical savedDossierMedical = dossierMedicalRepository.save(numberedDossierMedical);
		o.setDossierMedical(savedDossierMedical);
		try {
			patientRepository.save(o);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void update(Patient o) {
		patientRepository.save(o);
	}

	@Override
	public void delete(Patient o) {
		patientRepository.delete(o);
	}

	@Override
	public List<Patient> findAll() {
		return patientRepository.findAll();
	}

	@Override
	public Patient findById(long id) {
		return patientRepository.findById(id).get();
	}

}
