package com.clinital.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
@Slf4j
public class EmailSenderService {

	@Autowired
	private JavaMailSender javaMailSender;

	@Async
	public void sendMail(String userEmail, String confirmationToken) {
		try {
			MimeMessage mimeMessage = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(
					mimeMessage,"utf-8"
			);
			helper.setTo(userEmail);
			helper.setFrom("no-reply@clinital.io");
			helper.setSubject("Activation du compte clinital");
			helper.setText("Bonjour "
					+"Nous vous souhaiton la bienvenue sur la platforme Clinital"
					+"Pour confirmer votre compte, merci de cliquer sur le lien : "
					+ "http://localhost:8080/api/auth/confirmaccount?token=" + confirmationToken
					+ "   Note: le lien va expirer après 10 minutes.",true);
			javaMailSender.send(mimeMessage);
		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}

	public boolean sendSimpleMail(String to, String sub, String body) {
		log.info(body);
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(to);
		mailMessage.setFrom("");
		mailMessage.setSubject(sub);
		mailMessage.setText(body);
		Boolean isSent = false;
		try {
			javaMailSender.send(mailMessage);
			isSent = true;
		} catch (Exception e) {
			log.info(e.getMessage());
			e.printStackTrace();
		}

		return isSent;
	}
}
