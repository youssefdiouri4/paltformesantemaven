package com.clinital.services;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.clinital.dao.IDao;
import com.clinital.dto.RendezvousDTO;
import com.clinital.models.Rendezvous;
import com.clinital.repository.RdvRepository;
import com.clinital.util.ClinitalModelMapper;

@Service
@Primary
public class RendezvousService implements IDao<Rendezvous> {

	@Autowired
	private RdvRepository rdvrepo;

	@Autowired
	private ClinitalModelMapper mapper;

	@Override
	public void create(Rendezvous o) {
		rdvrepo.save(o);

	}

	@Override
	public void update(Rendezvous o) {
		rdvrepo.save(o);
	}

	@Override
	public void delete(Rendezvous o) {
		rdvrepo.delete(o);

	}

	@Override
	public List<Rendezvous> findAll() {
		return rdvrepo.findAll();
	}

	@Override
	public Rendezvous findById(long id) {
		return rdvrepo.findById(id).get();
	}

	public List<RendezvousDTO> findRendezvousByMedAndDate(Long medecinId, LocalDateTime fromDate) {
//		ZoneId systemTimeZone = ZoneId.systemDefault();

//		ZonedDateTime zonedDateTimeFrom = fromDate.atStartOfDay(systemTimeZone);
//		ZonedDateTime zonedDateTimeTo = toDate.atStartOfDay(systemTimeZone);
//
//		Date startDate = Date.from(zonedDateTimeFrom.toInstant());
//		Date endDate = Date.from(zonedDateTimeTo.toInstant());

		return rdvrepo.findByDateAndMedecin(fromDate, medecinId).stream()
				.map(rdv -> mapper.map(rdv, RendezvousDTO.class)).collect(Collectors.toList());

	}

}
