package com.clinital.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.clinital.models.User;
import com.clinital.payload.request.SignupRequest;
import com.clinital.repository.ConfirmationTokenRepository;
import com.clinital.repository.UserRepository;
import com.clinital.security.ConfirmationToken;

@Service
public class AutService {

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
	PasswordEncoder encoder;

    @Autowired
    private ConfirmationTokenRepository confirmationTokenRepository;
    
    public User findByEmail(String email) {
        return userRepository.findByEmail(email).get();        
    }

    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    public User save(User user){
        return userRepository.save(user);
    }

    public User saveUser(SignupRequest signUpRequest) {
        User user = new User();
        user.setEmail(signUpRequest.getEmail());
        user.setTelephone(signUpRequest.getTelephone());
        user.setPassword(signUpRequest.getPassword());
        user.setPassword(encoder.encode(user.getPassword()));
        return userRepository.save(user);
    }
    


    public boolean changePassword(String email, String password) {
        User user = findByEmail(email);
        user.setPassword(encoder.encode(password));
        if(save(user) != null) {
            return true;
        }
        return false;
    }

    public ConfirmationToken createToken(User user) {
        ConfirmationToken confirmationToken = new ConfirmationToken(user);
        return confirmationTokenRepository.save(confirmationToken);
    }
    public ConfirmationToken findByConfirmationToken(String token) {
        return confirmationTokenRepository.findByConfirmationToken(token);
    }
    public void deleteToken(ConfirmationToken confirmationToken) {
        this.confirmationTokenRepository.delete(confirmationToken);
    }
}