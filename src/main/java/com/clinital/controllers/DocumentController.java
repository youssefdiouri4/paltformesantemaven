package com.clinital.controllers;

import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.azure.storage.blob.BlobClient;
import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import com.azure.storage.common.StorageSharedKeyCredential;
import com.clinital.dto.DocumentDTO;
import com.clinital.dto.PatientDTO;
import com.clinital.models.Document;
import com.clinital.models.Patient;
import com.clinital.models.Rendezvous;
import com.clinital.models.User;
import com.clinital.payload.request.DocumentRequest;
import com.clinital.payload.response.ApiResponse;
import com.clinital.payload.response.DocumentResponse;
import com.clinital.payload.response.TypeDocumentResponse;
import com.clinital.repository.DocumentRepository;
import com.clinital.repository.PatientRepository;
import com.clinital.repository.RdvRepository;
import com.clinital.repository.TypeDocumentRepository;
import com.clinital.repository.UserRepository;
import com.clinital.security.jwt.JwtUtils;
import com.clinital.security.services.UserDetailsImpl;
import com.clinital.util.ClinitalModelMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.SharedAccessAccountPolicy;
import com.microsoft.azure.storage.StorageException;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/doc")
public class DocumentController {

	@Autowired
	DocumentRepository docrepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	JwtUtils jwtUtils;

	@Value(value = "${azure.storage.account-key}")
	String azureStorageToken;

	@Autowired
	ClinitalModelMapper mapper;

	@Autowired
	PatientRepository patientRepo;

	@Autowired
	TypeDocumentRepository typeDocumentRepo;

	@Autowired
	RdvRepository rdvRepository;

	@GetMapping("/documents")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	Iterable<DocumentResponse> documents() {
		UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		List<Patient> allPatients = patientRepo.findAllByUserId(userDetails.getId());
		return docrepository
				.findByPatientIdIn(allPatients.stream().map(patient -> patient.getId()).collect(Collectors.toList()))
				.stream().map(document -> mapper.map(document, DocumentResponse.class)).collect(Collectors.toList());

	}

	@GetMapping("/docById/{id}")
	public ResponseEntity<DocumentResponse> getDocById(@PathVariable("id") long id) {
		Optional<Document> tutorialData = docrepository.findById(id);

		if (tutorialData.isPresent()) {
			return new ResponseEntity<>(mapper.map(tutorialData.get(), DocumentResponse.class), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/docByIdPatient")
	@ResponseBody
	public List<DocumentResponse> findDocByIdPatient(@RequestParam("patient") Long patientId) {
		return docrepository.getDocByIdPatient(patientId).stream().map(doc -> mapper.map(doc, DocumentResponse.class))
				.collect(Collectors.toList());
	}

	@PostMapping("/docByIdRdv")
	@ResponseBody
	public List<DocumentResponse> findDocByIdRdv(@RequestParam("rdvId") Long rdvId) {
		return docrepository.getDocByIdRendezvous(rdvId).stream().map(doc -> mapper.map(doc, DocumentResponse.class))
				.collect(Collectors.toList());
	}

	@GetMapping("/docByNomPatient")
	@ResponseBody
	public List<DocumentResponse> findDocByNomPatient(@RequestParam("nomPatient") String nomPatient) {
		return docrepository.getDocByNomPatient(nomPatient).stream().map(doc -> mapper.map(doc, DocumentResponse.class))
				.collect(Collectors.toList());
	}

	@PostMapping(path = "/addDoc")
	@ResponseBody
	public ResponseEntity<?> addDoc(@RequestParam("document") String documentStr,
			@RequestParam("docFile") MultipartFile docFile) throws Exception {
		ObjectMapper om = new ObjectMapper();

		DocumentRequest document = om.readValue(documentStr, DocumentRequest.class);

		try {
			Document documentEntity = new Document();
			documentEntity.setTitre_doc(document.getTitre_doc());
			documentEntity.setTypeDoc(typeDocumentRepo.getOne(document.getTypeDocId()));
			Patient patient = patientRepo.getOne(document.getPatientId());
			documentEntity.setPatient(patient);
			documentEntity.setDossier(patient.getDossierMedical());
			documentEntity.setArchived(false);
			documentEntity.setDate_ajout_doc(new Date());
			Rendezvous rendezvous = null;
			if (document.getRdvId() != null) {
				rendezvous = rdvRepository.findById(document.getRdvId()).get();
				documentEntity.setRendezvous(rendezvous);
			}
			String accountName = "documentspatient";
			String accountKey = azureStorageToken;

			StorageSharedKeyCredential credential = new StorageSharedKeyCredential(accountName, accountKey);

			String endpoint = String.format(Locale.ROOT, "https://%s.blob.core.windows.net", accountName);

			BlobServiceClient blobServiceClient = new BlobServiceClientBuilder().endpoint(endpoint)
					.credential(credential).buildClient();

			BlobContainerClient containerClient = blobServiceClient.getBlobContainerClient("documentspatient");

			Document savedDoc = docrepository.save(documentEntity);

			String extension = FilenameUtils.getExtension(docFile.getOriginalFilename());

			BlobClient blobClient = containerClient.getBlobClient(savedDoc.getId_doc() + "." + extension);

			blobClient.upload(docFile.getInputStream(), docFile.getSize(), true);

			savedDoc.setFichier_doc(blobClient.getBlobUrl());

			documentEntity.setNumero_doc(savedDoc.getId_doc());
			Document finalSavedDoc = docrepository.save(documentEntity);

			if (document.getRdvId() != null) {

				rendezvous.getDocuments().add(finalSavedDoc);

				rdvRepository.save(rendezvous);

			}

			return ResponseEntity.ok(new ApiResponse(true, "Document created successfully!"));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new ApiResponse(true, "Document not created!"));

		}

	}

	@GetMapping("/allDocByPatientId")
	@ResponseBody
	public List<DocumentResponse> findAllDocsByPatientId() {

		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = userDetails.getUsername();

		User user = userRepository.findByEmail(username).orElse(null);

		List<Document> documents = docrepository.findByPatientId(user.getId());

		return documents.stream().map(doc -> mapper.map(doc, DocumentResponse.class)).collect(Collectors.toList());
	}

	@GetMapping("/allArchivedDocByPatientId")
	@ResponseBody
	public List<DocumentDTO> findAllArchivedDocsByPatientId() {

		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = userDetails.getUsername();

		User user = userRepository.findByEmail(username).orElse(null);

		List<Document> documents = docrepository.findByPatientIdAndArchived(user.getId(), true);

		return documents.stream().map(doc -> mapper.map(doc, DocumentDTO.class)).collect(Collectors.toList());
	}

	@GetMapping("/typeDocuments")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	Iterable<TypeDocumentResponse> getTypeDocuments() {

		return typeDocumentRepo.findAll().stream().map(typeDoc -> mapper.map(typeDoc, TypeDocumentResponse.class))
				.collect(Collectors.toList());

	}

	@GetMapping("/getSasToken")
	public ResponseEntity<?> getSAS() throws InvalidKeyException, URISyntaxException, StorageException {

		String storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=documentspatient;AccountKey="
				+ azureStorageToken + ";EndpointSuffix=core.windows.net";
		CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);

		SharedAccessAccountPolicy sharedAccessAccountPolicy = new SharedAccessAccountPolicy();
		sharedAccessAccountPolicy.setPermissionsFromString("racwdlup");
		long date = new Date().getTime();
		long expiryDate = new Date(date + 8640000).getTime();
		sharedAccessAccountPolicy.setSharedAccessStartTime(new Date(date));
		sharedAccessAccountPolicy.setSharedAccessExpiryTime(new Date(expiryDate));
		sharedAccessAccountPolicy.setResourceTypeFromString("sco");
		sharedAccessAccountPolicy.setServiceFromString("bfqt");
		String sasToken = storageAccount.generateSharedAccessSignature(sharedAccessAccountPolicy);

		return ResponseEntity.ok(new ApiResponse(true, sasToken));
	}

	@PostMapping("/getDocByPatientIdAndMedecin")
	@ResponseBody
	public List<DocumentResponse> getDocByPatientIdAndMedecin() {

		UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		List<Patient> allPatients = patientRepo.findAllByUserId(userDetails.getId());
		List<Document> documents = docrepository.getDocByPatientIdAndMedecin(
				allPatients.stream().map(patient -> patient.getId()).collect(Collectors.toList()));

		return documents.stream().map(doc -> mapper.map(doc, DocumentResponse.class)).collect(Collectors.toList());
	}

	@PostMapping(path = "/archiveDoc")
	public ResponseEntity<?> archiveDoc(@RequestParam("docId") Long docId, @RequestParam("archive") boolean archive)
			throws Exception {
		Document document = docrepository.getOne(docId);

		document.setArchived(archive);

		docrepository.save(document);

		return ResponseEntity.ok(new ApiResponse(true, "Archived"));

	}

	@PostMapping(path = "/deleteDoc")
	public ResponseEntity<?> deleteDoc(@RequestParam("docId") Long docId) throws Exception {
		Document document = docrepository.getOne(docId);

		docrepository.delete(document);

		return ResponseEntity.ok(new ApiResponse(true, "Deleted"));

	}

	public static class DocumentParams {

		public Long id_doc;
		public Long numero_doc;
		public String type_doc;
		public String titre_doc;
		public Date date_ajout_doc;
		public String auteur;
		public String fichier_doc;
		public PatientDTO patient;
	}

}
