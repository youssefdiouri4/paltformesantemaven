package com.clinital.controllers;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.azure.storage.blob.BlobClient;
import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import com.azure.storage.common.StorageSharedKeyCredential;
import com.clinital.enums.RdvStatutEnum;
import com.clinital.exception.BadRequestException;
import com.clinital.models.Document;
import com.clinital.models.Medecin;
import com.clinital.models.Patient;
import com.clinital.models.Rendezvous;
import com.clinital.models.TypeConsultation;
import com.clinital.payload.request.RendezvousRequest;
import com.clinital.payload.response.ApiResponse;
import com.clinital.payload.response.RendezvousResponse;
import com.clinital.repository.DocumentRepository;
import com.clinital.repository.MedecinRepository;
import com.clinital.repository.MedecinScheduleRepository;
import com.clinital.repository.PatientRepository;
import com.clinital.repository.RdvRepository;
import com.clinital.repository.TypeConsultationRepository;
import com.clinital.security.services.UserDetailsImpl;
import com.clinital.services.RendezvousService;
import com.clinital.util.ClinitalModelMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.sun.istack.NotNull;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/rdv")
public class RdvController {

	@Autowired
	RdvRepository rdvrepository;

	@Autowired
	RendezvousService rdvservice;

	@Autowired
	ClinitalModelMapper mapper;

	@Autowired
	MedecinRepository medRepo;

	@Autowired
	TypeConsultationRepository typeConsultRepo;

	@Autowired
	MedecinScheduleRepository medScheduleRepo;

	@Autowired
	PatientRepository patientRepo;

	@Autowired
	DocumentRepository docrepository;

	@Value(value = "${azure.storage.account-key}")
	String azureStorageToken;

	@GetMapping("/rdvs")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	Iterable<RendezvousResponse> rendezvous() {
		UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		try {
			return rdvrepository.findAll(userDetails.getId()).stream()
					.map(rdv -> mapper.map(rdv, RendezvousResponse.class)).collect(Collectors.toList());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@GetMapping("/rdvById/{id}")
	public ResponseEntity<RendezvousResponse> getRdvById(@PathVariable("id") Long id) {
		Optional<Rendezvous> tutorialData = rdvrepository.findById(id);

		if (tutorialData.isPresent()) {
			return new ResponseEntity<>(mapper.map(tutorialData.get(), RendezvousResponse.class), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/rdvByIdPatient")
	@ResponseBody
	public List<RendezvousResponse> findRdvByIdPatient(@RequestParam("id") Long id) {
		return rdvrepository.getRdvByIdPatient(id).stream().map(rdv -> mapper.map(rdv, RendezvousResponse.class))
				.collect(Collectors.toList());
	}

	@GetMapping("/rdvByNomPatient")
	@ResponseBody
	public List<RendezvousResponse> findRdvByNomPatient(@RequestParam("nomPatient") @NotNull String nomPatient) {
		return rdvrepository.getRdvByNomPatient(nomPatient).stream()
				.map(rdv -> mapper.map(rdv, RendezvousResponse.class)).collect(Collectors.toList());
	}

	@GetMapping("/rdvByDate")
	@ResponseBody
	public List<RendezvousResponse> rdvByDate(@RequestParam("jour") @NotNull Date jour) {
		return rdvrepository.getRdvByDate(jour).stream().map(rdv -> mapper.map(rdv, RendezvousResponse.class))
				.collect(Collectors.toList());
	}

//	@GetMapping("/rdvByMotif")
//	@ResponseBody
//	public List<RendezvousResponse> findRdvByMotif(@Valid @RequestBody RendezvousDTO rdvDetails) {
//		return rdvrepository.getRdvByMotif(rdvDetails.getMotif()).stream()
//				.map(rdv -> mapper.map(rdv, RendezvousResponse.class)).collect(Collectors.toList());
//	}

	@PostMapping("/addRdv")
	@ResponseBody
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	public void addRendezvous(@Valid @RequestBody RendezvousRequest c) throws BadRequestException {

		Medecin medecin = medRepo.findById(c.getMedecinId())
				.orElseThrow(() -> new BadRequestException("Medecin not found for this id :: " + c.getMedecinId()));

		TypeConsultation typeConsultation = typeConsultRepo.findById(c.getTypeConsultationId())
				.orElseThrow(() -> new BadRequestException(
						"Type Consultation not found for this id :: " + c.getTypeConsultationId()));

//		MedecinSchedule medecinSchedule = medScheduleRepo.findById(c.getMedecinScheduleId()).orElseThrow(
//				() -> new BadRequestException("Medecin Schedule not found for this id :: " + c.getMedecinScheduleId()));

		Patient patient = patientRepo.findById(c.getPatientId())
				.orElseThrow(() -> new BadRequestException("Patient not found for this id :: " + c.getPatientId()));

		Rendezvous rendezvous = mapper.map(c, Rendezvous.class);
		rendezvous.setMedecin(medecin);
		rendezvous.setTypeConsultation(typeConsultation);
		rendezvous.setPatient(patient);

//		Rendezvous savedRendezvouz = 
		rdvrepository.save(rendezvous);

//		medecinSchedule.getRendevouz().add(savedRendezvouz);

//		medScheduleRepo.save(medecinSchedule);

	}

	@DeleteMapping("/delete/{id}")
	public Map<String, Boolean> deleteRdv(@PathVariable(value = "id") Long id) throws BadRequestException {
		Rendezvous rdv = rdvrepository.findById(id)
				.orElseThrow(() -> new BadRequestException("Rendez-vous not found for this id :: " + id));

		rdvrepository.delete(rdv);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

	@PostMapping("/updateRdv")
	public ResponseEntity<RendezvousResponse> updateRdv(@Valid @RequestBody RendezvousRequest rdvDetails)
			throws BadRequestException {
		try {
			Rendezvous rdv = rdvrepository.findById(rdvDetails.getId()).orElseThrow(
					() -> new BadRequestException("Rendez-vous not found for this id :: " + rdvDetails.getId()));

			Patient patient = patientRepo.findById(rdvDetails.getPatientId()).orElseThrow(
					() -> new BadRequestException("Patient not found for this id :: " + rdvDetails.getPatientId()));

			Medecin medecin = medRepo.findById(rdvDetails.getMedecinId()).orElseThrow(
					() -> new BadRequestException("Medecin not found for this id :: " + rdvDetails.getMedecinId()));

			TypeConsultation typeConsultation = typeConsultRepo.findById(rdvDetails.getTypeConsultationId())
					.orElseThrow(() -> new BadRequestException(
							"Type Consultation not found for this id :: " + rdvDetails.getTypeConsultationId()));
			rdv.setId(rdvDetails.getId());
//		rdv.setMotif(rdvDetails.getMotif());
//		rdv.setCanceledAt(rdvDetails.getCanceledAt());
			rdv.setDay(rdvDetails.getJour());
			rdv.setStart(rdvDetails.getStart());
			rdv.setEnd(rdvDetails.getStart().plusMinutes(typeConsultation.getPeriod().getValue()));
			rdv.setPatient(mapper.map(patient, Patient.class));
			rdv.setMedecin(mapper.map(medecin, Medecin.class));
			final Rendezvous updatedrdv = rdvrepository.save(rdv);
			return ResponseEntity.ok(mapper.map(updatedrdv, RendezvousResponse.class));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(null);

		}
	}

	@GetMapping("/rdvs/medecin/{idmed}")
	Iterable<RendezvousResponse> rendezvousForMedecin(@PathVariable(value = "idmed") Long idmed) {
		return rdvrepository.findByAllRdvByMedecin(idmed).stream().map(rdv -> mapper.map(rdv, RendezvousResponse.class))
				.collect(Collectors.toList());
	}

	@GetMapping("/rdvs/secretaire/{idsec}")
	Iterable<RendezvousResponse> rendezvousForSecretaire(@PathVariable(value = "idsec") Long idsec) {
		return rdvrepository.findByMedecinCabinetSecretairesId(idsec).stream()
				.map(rdv -> mapper.map(rdv, RendezvousResponse.class)).collect(Collectors.toList());
	}

	@PostMapping("/cancelRdv")
	public ResponseEntity<RendezvousResponse> cancelRdv(@RequestParam(value = "id") Long id)
			throws BadRequestException {
		Rendezvous rdv = rdvrepository.findById(id)
				.orElseThrow(() -> new BadRequestException("Rendez-vous not found for this id :: " + id));

		rdv.setCanceledAt(LocalDateTime.now());
		rdv.setStatut(RdvStatutEnum.ANNULE);
		final Rendezvous updatedrdv = rdvrepository.save(rdv);
		return ResponseEntity.ok(mapper.map(updatedrdv, RendezvousResponse.class));
	}

	@PostMapping(path = "/uploadDocRdv")
	@ResponseBody
	public ResponseEntity<?> uploadDocRdv(@RequestParam("id") Long id, @RequestParam("docFile") MultipartFile docFile)
			throws Exception {
		ObjectMapper om = new ObjectMapper();

		String accountName = "documentspatient";
		String accountKey = azureStorageToken;

		StorageSharedKeyCredential credential = new StorageSharedKeyCredential(accountName, accountKey);

		String endpoint = String.format(Locale.ROOT, "https://%s.blob.core.windows.net", accountName);

		BlobServiceClient blobServiceClient = new BlobServiceClientBuilder().endpoint(endpoint).credential(credential)
				.buildClient();

		BlobContainerClient containerClient = blobServiceClient.getBlobContainerClient("documentspatient");

		Rendezvous rendezvous = rdvrepository.getOne(id);
		Document doc = new Document();

		doc.setDate_ajout_doc(new Date());
		doc.setPatient(rendezvous.getPatient());
		doc.setDossier(rendezvous.getPatient().getDossierMedical());

		Document savedDoc = docrepository.save(doc);

		String extension = FilenameUtils.getExtension(docFile.getOriginalFilename());

		BlobClient blobClient = containerClient.getBlobClient(savedDoc.getId_doc() + "." + extension);

		blobClient.upload(docFile.getInputStream(), docFile.getSize(), true);

		savedDoc.setFichier_doc(blobClient.getBlobUrl());

		docrepository.save(savedDoc);

//		rendezvous.getDocuments().add(savedDoc);

		rdvrepository.save(rendezvous);

		return ResponseEntity.ok(new ApiResponse(true, "Document uploaded!"));

	}

//	  public static class RendezvousParams {
//	   
//	    	public long id;
//	    	public Date jour;
//	    	public String motif;
//	    	public Medecin medecin;
//	    	public Patient patient;
//	    }

}
