package com.clinital.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.clinital.dto.TypeConsultationDTO;
import com.clinital.exception.BadRequestException;
import com.clinital.models.TypeConsultation;
import com.clinital.payload.request.TypeConsultationRequest;
import com.clinital.payload.response.TypeConsultationResponse;
import com.clinital.repository.TypeConsultationRepository;
import com.clinital.security.services.UserDetailsImpl;
import com.clinital.util.ClinitalModelMapper;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/typeConsultation")
public class TypeConsultationController {

	@Autowired
	TypeConsultationRepository typeConsultationRepository;

	@Autowired
	ClinitalModelMapper mapper;

	@GetMapping("/getAllByMedecinId/{id}")
	@ResponseBody
	public List<TypeConsultationResponse> findAllTypeConsultationByMedecinId(
			@PathVariable("medecinId") Long medecinId) {

		UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		if (userDetails.getId() == medecinId)
			return typeConsultationRepository.findByMedecinId(medecinId).stream()
					.map(consult -> mapper.map(consult, TypeConsultationResponse.class)).collect(Collectors.toList());
		else
			return null;
	}

	@GetMapping("/getTypeConsultationById/{id}")
	@ResponseBody
	public TypeConsultationResponse findTypeConsultationById(@PathVariable("id") Long id) {

		TypeConsultationResponse typeConsultationResponse = null;
		UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		typeConsultationResponse = mapper.map(
				typeConsultationRepository.findById(id)
						.orElseThrow(() -> new BadRequestException("Type Consultation not found for this id :: " + id)),
				TypeConsultationResponse.class);

		if (userDetails.getId() == typeConsultationResponse.getMedecinId())
			return typeConsultationResponse;
		else
			return null;
	}

	@PostMapping("/addTypeConsultation")
	@ResponseBody
	public void addTypeConsultation(@Valid @RequestBody TypeConsultationRequest typeConsultation) {

		typeConsultationRepository.save(mapper.map(typeConsultation, TypeConsultation.class));
	}

	@DeleteMapping("/delete/{id}")
	public Map<String, Boolean> deleteTypeConsultation(@PathVariable(value = "id") Long id) throws BadRequestException {
		Map<String, Boolean> response = new HashMap<>();

		UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		TypeConsultation typeConsultation = typeConsultationRepository.findById(id)
				.orElseThrow(() -> new BadRequestException("Type Consultation not found for this id :: " + id));

		if (userDetails.getId() == typeConsultation.getMedecin().getId()) {
			typeConsultationRepository.delete(typeConsultation);
			response.put("deleted", Boolean.TRUE);
		} else

		{
			response.put("not deleted", Boolean.FALSE);
		}

		return response;
	}

	@PutMapping("/updateTypeConsultation")
	public ResponseEntity<TypeConsultationDTO> updateTypeConsultation(
			@Valid @RequestBody TypeConsultationRequest typeConsultation) throws BadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		if (userDetails.getId() == typeConsultation.getMedecinId()) {

			TypeConsultation typeConsultationUpdated = typeConsultationRepository
					.save(mapper.map(typeConsultation, TypeConsultation.class));

			return ResponseEntity.ok(mapper.map(typeConsultationUpdated, TypeConsultationDTO.class));
		} else
			throw new BadRequestException("Not allowed");
	}

}
