package com.clinital.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.clinital.enums.PatientTypeEnum;
import com.clinital.exception.BadRequestException;
import com.clinital.models.Patient;
import com.clinital.models.User;
import com.clinital.models.Ville;
import com.clinital.payload.request.PatientRequest;
import com.clinital.payload.response.PatientResponse;
import com.clinital.repository.PatientRepository;
import com.clinital.repository.UserRepository;
import com.clinital.repository.VilleRepository;
import com.clinital.security.services.UserDetailsImpl;
import com.clinital.services.PatientService;
import com.clinital.util.ClinitalModelMapper;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/patient")
public class PatientController {

	@Autowired
	PatientRepository patientRepo;

	@Autowired
	PatientService patientService;

	@Autowired
	ClinitalModelMapper mapper;

	@Autowired
	UserRepository userRepo;

	@Autowired
	VilleRepository villeRepository;

	@GetMapping("/getall")
	@ResponseBody
	public List<PatientResponse> findAllPatients() {
		UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		return patientRepo.findAllByUserId(userDetails.getId()).stream()
				.map(pat -> mapper.map(pat, PatientResponse.class)).collect(Collectors.toList());
	}

	@GetMapping("/getPatientById/{account}")
	@ResponseBody
	public PatientResponse findPatientByAccount(@PathVariable("account") Long userID) {
		return mapper.map(patientRepo.findPatientByAccount(userID), PatientResponse.class);
	}

	@GetMapping("/getall/type/{type}")
	@ResponseBody
	public List<PatientResponse> findPatientByType(@PathVariable("type") PatientTypeEnum type) {
		return patientRepo.findPatientByType(type).stream().map(pat -> mapper.map(pat, PatientResponse.class))
				.collect(Collectors.toList());
	}

	@PostMapping("/addpatient")
	@ResponseBody
	public void addPatient(@Valid @RequestBody PatientRequest patient) throws BadRequestException {
		Patient patientEntity = mapper.map(patient, Patient.class);
		UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		Ville ville = villeRepository.findById(patient.getVilleId())
				.orElseThrow(() -> new BadRequestException("Ville not found for this id :: " + patient.getVilleId()));

		patientEntity.setUser(userRepo.getOne(userDetails.getId()));
		patientEntity.setVille(ville);

		patientService.create(patientEntity);
	}

	@DeleteMapping("/delete/{id}")
	public Map<String, Boolean> deletePatient(@PathVariable(value = "id") Long id) throws BadRequestException {
		Patient patient = patientRepo.findById(id)
				.orElseThrow(() -> new BadRequestException("Patient not found for this id :: " + id));

		patientService.delete(patient);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

	@PostMapping("/updatepatient")
	public ResponseEntity<PatientResponse> updatePatient(@RequestParam(value = "id") Long id,
			@Valid @RequestBody PatientRequest patient) throws BadRequestException {
		Patient pt = patientRepo.findById(id)
				.orElseThrow(() -> new BadRequestException("Patient not found for this id :: " + id));

		UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		Ville ville = villeRepository.findById(patient.getVilleId())
				.orElseThrow(() -> new BadRequestException("Ville not found for this id :: " + patient.getVilleId()));

		pt.setNom_pat(patient.getNom_pat());
		pt.setPrenom_pat(patient.getPrenom_pat());
		pt.setDateNaissance(patient.getDateNaissance());
		pt.setAdresse_pat(patient.getAdresse_pat());
		pt.setCodePost_pat(patient.getCodePost_pat());
		pt.setMatricule_pat(patient.getMatricule_pat());
		pt.setCivilite_pat(patient.getCivilite_pat());
		pt.setVille(ville);
		pt.setUser(mapper.map(userRepo.getOne(userDetails.getId()), User.class));
		pt.setPatient_type(patient.getPatient_type());
		Patient updatedpt = null;
		try {
			updatedpt = patientRepo.save(pt);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(mapper.map(updatedpt, PatientResponse.class));
	}

}
