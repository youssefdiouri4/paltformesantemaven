package com.clinital.controllers;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.clinital.dto.RendezvousDTO;
import com.clinital.dto.SpecialiteDTO;
import com.clinital.dto.VilleDTO;
import com.clinital.models.Medecin;
import com.clinital.models.MedecinSchedule;
import com.clinital.models.TypeConsultation;
import com.clinital.payload.response.AgendaResponse;
import com.clinital.payload.response.GeneralResponse;
import com.clinital.payload.response.HorairesResponse;
import com.clinital.payload.response.MedecinResponse;
import com.clinital.payload.response.TypeConsultationResponse;
import com.clinital.repository.MedecinRepository;
import com.clinital.repository.MedecinScheduleRepository;
import com.clinital.repository.TypeConsultationRepository;
import com.clinital.services.RendezvousService;
import com.clinital.util.ClinitalModelMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/med")
@Slf4j
public class MedecinController {

	@Autowired
	MedecinRepository medrepository;

	@Autowired
	ClinitalModelMapper mapper;

	@Autowired
	RendezvousService rendezvousService;

	@Autowired
	MedecinScheduleRepository medScheduleRepo;

	@Autowired
	TypeConsultationRepository typeConsultationRepo;

	@GetMapping("/medecins")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	Iterable<MedecinResponse> medecins() {
		return medrepository.findAll().stream().map(med -> mapper.map(med, MedecinResponse.class))
				.collect(Collectors.toList());
	}

	@GetMapping("/medById/{id}")
	public ResponseEntity<MedecinResponse> getMedecinById(@PathVariable("id") Long id) {
		Optional<Medecin> tutorialData = medrepository.findById(id);

		if (tutorialData.isPresent()) {
			return new ResponseEntity<>(mapper.map(tutorialData.get(), MedecinResponse.class), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/medByName")
	@ResponseBody
	public Iterable<MedecinResponse> findMedByName(@RequestParam String nomMed) {
		return medrepository.getMedecinByName(nomMed).stream().map(med -> mapper.map(med, MedecinResponse.class))
				.collect(Collectors.toList());
	}

	@GetMapping("/medByNameOrSpecAndVille")
	@ResponseBody
	public Iterable<MedecinResponse> medByNameOrSpecAndVille(@RequestParam("ville") String ville,
			@RequestParam("search") String search) {
		return medrepository.getMedecinBySpecialiteOrNameAndVille(ville, search).stream()
				.map(med -> mapper.map(med, MedecinResponse.class)).collect(Collectors.toList());
	}

	@GetMapping("/medByNameAndSpec")
	public Iterable<MedecinResponse> findMedSpecNameVille(@RequestParam("ville") String ville,
			@RequestParam("search") String search) {
		return medrepository.getMedecinBySpecialiteOrName(ville, search).stream()
				.map(med -> mapper.map(med, MedecinResponse.class)).collect(Collectors.toList());
	}

	@GetMapping("/medByNameOrSpec")
	public Iterable<MedecinResponse> findMedSpecName(@RequestParam("search") String search) {
		return medrepository.getMedecinBySpecOrName(search).stream().map(med -> mapper.map(med, MedecinResponse.class))
				.collect(Collectors.toList());

	}

	@GetMapping("/medByVille")
	public Iterable<MedecinResponse> findMedByVille(@RequestParam("id_ville") Long id_ville) {
		return medrepository.getMedecinByVille(id_ville).stream().map(med -> mapper.map(med, MedecinResponse.class))
				.collect(Collectors.toList());

	}

	@PostMapping("/agenda")
	@JsonSerialize(using = LocalDateSerializer.class)
	public List<AgendaResponse> findAvailableTimes(@RequestParam("idmed") Long idmed,
			@RequestParam("idconsult") Long idconsult,
			@RequestParam(name = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @ApiParam(value = "startDate", example = "yyyy-MM-dd") LocalDate startDate) {
		List<AgendaResponse> agendaResponseList = new ArrayList<AgendaResponse>();
		try {
			TypeConsultation typeConsultation = typeConsultationRepo.findById(idconsult).get();

			for (int i = 1; i <= 7; i++) {
				AgendaResponse agendaResponse = new AgendaResponse();

				agendaResponse.setDay(DayOfWeek.of(i));
				LocalDate nowDate = LocalDate.now();
				if (startDate != null)
					nowDate = startDate;

				if (nowDate.getDayOfWeek().getValue() <= i) {
					log.info("Found Day Greater than");
					List<MedecinSchedule> medecinSchedule = medScheduleRepo
							.findByMedecinIdAndTypeConsultationConsultationIdAndDay(idmed, idconsult, DayOfWeek.of(i));

					for (MedecinSchedule schedule : medecinSchedule) {

						long minutes = ChronoUnit.MINUTES.between(schedule.getAvailabilityStart(),
								schedule.getAvailabilityEnd());

						long totalSlots = minutes / typeConsultation.getPeriod().getValue();

						log.info("Total Slots " + totalSlots);
						LocalDateTime timer = schedule.getAvailabilityStart();

						if (startDate != null) {
							timer = timer.of(startDate.getYear(), startDate.getMonth(), startDate.getDayOfMonth(),
									timer.getHour(), timer.getMinute());
						}
						log.info("Timer Start:" + timer);

						for (int j = 0; j < totalSlots; j++) {
							log.info(timer.toString());

							List<RendezvousDTO> rendezvousList = rendezvousService.findRendezvousByMedAndDate(idmed,
									timer);

							if (rendezvousList.isEmpty()) {
								agendaResponse.getAvailableSlot().add(timer.getHour() + ":" + timer.getMinute());
							}

							timer = timer.plusMinutes(typeConsultation.getPeriod().getValue());

						}

					}
					{
						log.info("Empty Day Schedule");
					}

				} else {
					log.info("Later Days");
				}

				// Getting Medecin General Time Table
				List<MedecinSchedule> medecinSchedule = medScheduleRepo
						.findByMedecinIdAndTypeConsultationConsultationIdAndDay(idmed, idconsult, DayOfWeek.of(i));

				for (MedecinSchedule schedule : medecinSchedule) {

					String date = "";
					if (startDate != null) {
						TemporalField fieldISO = WeekFields.of(Locale.FRANCE).dayOfWeek();
						date = startDate.with(fieldISO, i).toString();

					} else {
						TemporalField fieldISO = WeekFields.of(Locale.FRANCE).dayOfWeek();
						date = LocalDate.now().with(fieldISO, i).toString();

					}

					String startDateTime = date + "T" + schedule.getAvailabilityStart().getHour() + ":"
							+ schedule.getAvailabilityStart().getMinute();

					String endDateTime = date + "T" + schedule.getAvailabilityEnd().getHour() + ":"
							+ schedule.getAvailabilityEnd().getMinute();

					agendaResponse.getMedecinTimeTable().add(new GeneralResponse("startTime", startDateTime));
					agendaResponse.getMedecinTimeTable().add(new GeneralResponse("endTime", endDateTime));

					String startTime = schedule.getAvailabilityStart().getHour() + ":"
							+ schedule.getAvailabilityStart().getMinute();

					String endTime = schedule.getAvailabilityEnd().getHour() + ":"
							+ schedule.getAvailabilityEnd().getMinute();

					agendaResponse.getWorkingHours().add(new HorairesResponse(startTime, endTime));

				}

				agendaResponseList.add(agendaResponse);
			}
		} catch (Exception e) {
			log.info("Warning mapping issue", e.getMessage());
		}

		return agendaResponseList;

	}

	@GetMapping("/getTypeConsultationById/{id}")
	@ResponseBody
	public List<TypeConsultationResponse> findTypeConsultationById(@PathVariable("id") Long id) {

		return typeConsultationRepo.findByMedecinId(id).stream()
				.map(typeConsult -> mapper.map(typeConsult, TypeConsultationResponse.class))
				.collect(Collectors.toList());
	}

	public static class MedecinParams {

		public Long id;
		public String nom_med;
		public String prenom_med;
		public String photo_med;
		public String diplome_med;
		public String experience_med;
		public String description_med;
		public VilleDTO ville;
		public SpecialiteDTO specialite;
	}

}
