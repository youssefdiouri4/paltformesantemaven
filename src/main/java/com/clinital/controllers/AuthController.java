package com.clinital.controllers;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import javax.mail.internet.MimeMessage;
import javax.validation.Valid;

import com.clinital.enums.CiviliteEnum;
import com.clinital.enums.PatientTypeEnum;
import com.clinital.models.*;
import com.clinital.repository.VilleRepository;
import com.clinital.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.clinital.enums.ERole;
import com.clinital.enums.ProviderEnum;
import com.clinital.exception.BadRequestException;
import com.clinital.payload.request.LoginRequest;
import com.clinital.payload.request.SignupRequest;
import com.clinital.payload.request.VerifyEmailRequest;
import com.clinital.payload.response.ApiResponse;
import com.clinital.payload.response.JwtResponse;
import com.clinital.payload.response.MessageResponse;
import com.clinital.payload.response.FacebookResponse;
import com.clinital.payload.response.GoogleResponse;
import com.clinital.repository.RoleRepository;
import com.clinital.repository.UserRepository;
import com.clinital.security.ConfirmationToken;
import com.clinital.security.jwt.JwtUtils;
import com.clinital.security.services.UserDetailsImpl;
import com.clinital.security.services.UserDetailsServiceImpl;
import com.clinital.services.AutService;
import com.clinital.services.EmailSenderService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
@Slf4j
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	AutService authService;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	UserDetailsServiceImpl userDetService;

	@Autowired
	JavaMailSender javaMailSender;

	@Autowired
	EmailSenderService emailSenderService;

	@Autowired
	PatientService patientService;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());

		User user = userRepository.findById(userDetails.getId()).get();

		if (user.getEmailVerified() == true) {

			userDetService.updateLastLoginDate(userDetails.getId());

			return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getId(), userDetails.getEmail(),
					userDetails.getTelephone(), roles));
		} else {
			return ResponseEntity.ok(new ApiResponse(false, "Email Not Verified"));
		}
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
		}


		// Create new user's account
		User user = new User(signUpRequest.getEmail(), signUpRequest.getTelephone(),
				encoder.encode(signUpRequest.getPassword()), null);


		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);

					break;
				case "mod":
					Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(modRole);

					break;
				default:
					Role userRole = roleRepository.findByName(ERole.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}



		// creation patient "MOI" automatiquement
		Patient patient = new Patient();
		patient.setNom_pat(null);
		patient.setPrenom_pat(null);
		patient.setDateNaissance(null);
		patient.setAdresse_pat(null);
		patient.setCodePost_pat(null);
		patient.setMatricule_pat(null);
		patient.setCivilite_pat(null);
		Ville ville = new Ville();
		ville.setId_ville(1L);
		patient.setVille(ville);
		patient.setDossierMedical(new DossierMedical());
		patient.setPatient_type(PatientTypeEnum.MOI);
		User user1 = new User();
		user1.setId(1L);
		patient.setUser(user1);
		patientService.create(patient);

		user.setRoles(roles);
		userRepository.save(user);
		ConfirmationToken token = authService.createToken(user);
		emailSenderService.sendMail(user.getEmail(), token.getConfirmationToken());
		return ResponseEntity.ok(new ApiResponse(true, "User registered successfully et un lien de verification a ete enovyé par mail "));
	}

	@GetMapping("confirmaccount")
	public ResponseEntity<?> getMethodName(@RequestParam("token") String token) {

		ConfirmationToken confirmationToken = authService.findByConfirmationToken(token);

		if (confirmationToken == null) {
			throw new BadRequestException("Invalid token");
		}

		User user = confirmationToken.getUser();
		Calendar calendar = Calendar.getInstance();

		if ((confirmationToken.getExpiryDate().getTime() - calendar.getTime().getTime()) <= 0) {
			return ResponseEntity.badRequest()
					.body("Lien expiré, generez un nouveau lien http://localhost:4200/signin");
		}

		user.setEmailVerified(true);
		authService.save(user);
		return ResponseEntity.ok("Account verified successfully!");
	}

	@PostMapping("/resetpassword")
	public ResponseEntity<?> resetPassword(@Valid @RequestBody LoginRequest loginRequest) {
		if (authService.existsByEmail(loginRequest.getEmail())) {
			if (authService.changePassword(loginRequest.getEmail(), loginRequest.getPassword())) {
				return ResponseEntity.ok(new ApiResponse(true, "Password changed successfully"));
			} else {
				return ResponseEntity.badRequest().body(new MessageResponse("Unable to change password. Try again!"));
			}

		} else {
			return ResponseEntity.badRequest().body(new MessageResponse("User not found with this email"));
		}

	}

	/*@PostMapping("/sendmail")
	public ResponseEntity<?> sendVerificationMail(@Valid @RequestBody VerifyEmailRequest emailRequest) {
		if (authService.existsByEmail(emailRequest.getEmail())) {
			if (userDetService.isAccountVerified(emailRequest.getEmail())) {
				throw new BadRequestException("Email est déja verifié ");
			} else {
				User user = authService.findByEmail(emailRequest.getEmail());
				ConfirmationToken token = authService.createToken(user);
				emailSenderService.sendMail(user.getEmail(), token.getConfirmationToken());
				return ResponseEntity.ok(new ApiResponse(true, "un lien de verification a ete enovyé par mail "));
			}
		} else {
			throw new BadRequestException("Email non associé ");
		}*/


	@PostMapping("/fbLogin")
	public ResponseEntity<?> getFacebookProfileInfo(final String accessToken) throws BadRequestException {
		log.debug("Calling Facebook API to validate and get profile info");
		RestTemplate restTemplate = new RestTemplate();
		// field names which will be retrieved from facebook
		final String fields = "id,email,first_name,last_name";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

			UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString("https://graph.facebook.com/me")
					.queryParam("access_token", accessToken).queryParam("fields", fields);

			log.debug("Facebook profile uri {}", uriBuilder.toUriString());
			FacebookResponse socialResponse = restTemplate.getForObject(uriBuilder.toUriString(),
					FacebookResponse.class);

			log.info("Facebook user authenticated and profile fetched successfully, details [{}]",
					socialResponse.toString());

			userDetService.processOAuthPostLogin(socialResponse.getEmail(), ProviderEnum.FACEBOOK);

			UserDetails userDetails = userDetService.loadUserByUsername(socialResponse.getEmail());

			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails,
					null, userDetails.getAuthorities());
			authentication.setDetails(new WebAuthenticationDetailsSource());

			SecurityContextHolder.getContext().setAuthentication(authentication);
			String jwt = jwtUtils.generateJwtToken(authentication);

			UserDetailsImpl userDetailsImpl = (UserDetailsImpl) authentication.getPrincipal();
			List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
					.collect(Collectors.toList());

			return ResponseEntity.ok(new JwtResponse(jwt, userDetailsImpl.getId(), userDetailsImpl.getEmail(),
					userDetailsImpl.getTelephone(), roles));
		} catch (HttpClientErrorException e) {
			log.error("Not able to authenticate from Facebook");
			return ResponseEntity.badRequest().body(new MessageResponse("Invalid access token"));

		} catch (Exception exp) {
			log.error("User is not authorized to login into system", exp);
			return ResponseEntity.badRequest().body(new MessageResponse("Invalid user"));

		}
	}

	@PostMapping("/googleLogin")
	public ResponseEntity<?> getGoogleTokenInfo(String accessToken) throws BadRequestException {
		log.debug("Calling Google API to get token info");
		RestTemplate restTemplate = new RestTemplate();
		GoogleResponse googleResponse = null;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

			UriComponentsBuilder uriBuilder = UriComponentsBuilder
					.fromUriString("https://www.googleapis.com/oauth2/v3/userinfo")
					.queryParam("access_token", accessToken);
			log.debug("google login uri {}", uriBuilder.toUriString());
			googleResponse = restTemplate.getForObject(uriBuilder.toUriString(), GoogleResponse.class);
			log.info("Gmail user authenticated successfully, details [{}]", googleResponse.toString());

			userDetService.processOAuthPostLogin(googleResponse.getEmail(), ProviderEnum.FACEBOOK);

			UserDetails userDetails = userDetService.loadUserByUsername(googleResponse.getEmail());

			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails,
					null, userDetails.getAuthorities());
			authentication.setDetails(new WebAuthenticationDetailsSource());

			SecurityContextHolder.getContext().setAuthentication(authentication);
			String jwt = jwtUtils.generateJwtToken(authentication);

			UserDetailsImpl userDetailsImpl = (UserDetailsImpl) authentication.getPrincipal();
			List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
					.collect(Collectors.toList());

			return ResponseEntity.ok(new JwtResponse(jwt, userDetailsImpl.getId(), userDetailsImpl.getEmail(),
					userDetailsImpl.getTelephone(), roles));

		} catch (HttpClientErrorException e) {
			log.error("Not able to authenticate from Google");
			try {
				JsonNode error = new ObjectMapper().readValue(e.getResponseBodyAsString(), JsonNode.class);
				log.error(error.toString());
				return ResponseEntity.badRequest().body(new MessageResponse("Invalid access token"));
			} catch (IOException mappingExp) {
				return ResponseEntity.badRequest().body(new MessageResponse("Invalid user"));
			}
		} catch (Exception exp) {
			log.error("User is not authorized to login into system", exp);
			return ResponseEntity.badRequest().body(new MessageResponse("Invalid user"));
		}
	}

}
