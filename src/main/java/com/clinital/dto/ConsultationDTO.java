package com.clinital.dto;

import java.util.Date;

import lombok.Data;

@Data
public class ConsultationDTO {
	private Long id_consultation;
	private String num_consultation;
	private Date date;
	private MedecinDTO medecin;
	private PatientDTO patient;


}
