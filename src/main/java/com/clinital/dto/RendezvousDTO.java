package com.clinital.dto;

import java.time.DayOfWeek;
import java.time.LocalDateTime;

import com.clinital.enums.RdvStatutEnum;

import lombok.Data;

@Data
public class RendezvousDTO {

	private Long id;
	private DayOfWeek day;
//	@NotBlank(message = "{validation.value.required}")
//	private String motif;
//	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ", shape = JsonFormat.Shape.STRING)
//	@JsonSerialize(using = LocalDateTimeSerializer.class)
//	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime start;
//	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ", shape = JsonFormat.Shape.STRING)
//	@JsonSerialize(using = LocalDateTimeSerializer.class)
//	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime end;
	private LocalDateTime canceledAt;
	private RdvStatutEnum statut;
	private MedecinDTO medecin;
	private PatientDTO patient;
	private TypeConsultationDTO typeConsultation;
//	private MedecinSchedule medecinSchedule;

}
