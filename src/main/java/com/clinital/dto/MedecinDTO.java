package com.clinital.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.clinital.enums.CiviliteEnum;

import lombok.Data;

@Data
public class MedecinDTO extends UserDTO {

	private Long id;

	@NotBlank
	private String matricule_med;
	@NotBlank
	private String nom_med;
	@NotBlank
	private String prenom_med;
	@NotBlank
	private String photo_med;
	@NotNull
	private String diplome_med;
	@NotNull
	private String experience_med;
	@NotNull
	private String description_med;
	private CiviliteEnum civilite_med;
	private VilleDTO ville;
	private SpecialiteDTO specialite;
	private List<RendezvousDTO> lesrdvs;
	private CabinetDTO cabinet;
	private List<MoyenPaiementDTO> moyenPaiement;

}
