package com.clinital.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class SecretaireDTO {
	@NotNull
	private String nom;
	@NotNull
	private String prenom;
	@NotNull
	private Date dateNaissance;
	@NotNull
	private String adresse;

	private CabinetDTO cabinet;
}
