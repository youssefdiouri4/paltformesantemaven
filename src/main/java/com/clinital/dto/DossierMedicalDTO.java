package com.clinital.dto;

import java.util.List;

import javax.persistence.OneToMany;

import com.clinital.enums.PatientTypeEnum;

import lombok.Data;

@Data
public class DossierMedicalDTO {

	private Long id_dossier;
	private String numDossier;
	private boolean traitement;
	private PatientTypeEnum dossierType;
	private List<DocumentDTO> documents;

}
