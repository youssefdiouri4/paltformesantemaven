package com.clinital.dto;

import com.clinital.enums.ConsultationPeriodEnum;

import lombok.Data;

@Data
public class TypeConsultationDTO {
	private Long consultationId;
	private String title;
	private ConsultationPeriodEnum period;
	private MedecinDTO medecin;

}
