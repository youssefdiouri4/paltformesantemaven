package com.clinital.enums;

public enum ConsultationPeriodEnum {

	MIN10(10), MIN20(20), MIN30(30);

	int value;

	ConsultationPeriodEnum(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
